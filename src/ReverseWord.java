public class ReverseWord {
    public static String reverse(String s){
        String output = "";
        StackX theStack = new StackX(s.length());
        for (int i = 0; i < s.length(); i++) {
            theStack.push(s.charAt(i));
        }
        while (!theStack.isEmpty()){
            output += theStack.popStr();
        }
        return output;
    }
}
