public class StackX {
    private int maxSize;
    private long[] stackLong;
    private char[] stackString;
    private int top;
    
    StackX(int size) {
        maxSize = size;
        stackLong = new long[maxSize];
        stackString = new char[maxSize];
        top = -1;
    }

    public void push(long value) {
        stackLong[++top] = value;
    }
    public void push(char value) {
        stackString[++top] = value;
    }
    public long pop() {
        return stackLong[top--];
    }
    public char popStr() {
        return stackString[top--];
    }

    public long peek() {
        return stackLong[top];
    }

    public boolean isEmpty() {
        return top == -1;
    }
    public boolean isFull(){
        return top == maxSize-1;
    }

}

